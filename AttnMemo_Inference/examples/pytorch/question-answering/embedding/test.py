# %% Imports
from math import cos, frexp
from typing_extensions import OrderedDict
from datasets.load import load_dataset, load_metric
import torch
import numpy as np
from numpy.linalg import norm
import transformers
from transformers import PreTrainedTokenizerFast
from torch import tensor

from transformers import modeling_utils
from transformers.models.auto.modeling_auto import AutoModelForQuestionAnswering
from transformers.models.auto.tokenization_auto import AutoTokenizer
import matplotlib.pyplot as plt
from transformers.models.bert.modeling_bert import *
from transformers import BertConfig, AutoModelForQuestionAnswering
from transformers import AutoTokenizer
from transformers.models.bert.tokenization_bert import BertTokenizer
from transformers.trainer_utils import EvalPrediction

def cos_sim(A, B):
    return np.dot(A, B)/(norm(A) * norm(B))

# %% Load The Model and its utils 

tokenizer = AutoTokenizer.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    use_fast=True,
    use_auth_token=True
    )
model_config = BertConfig.from_pretrained(pretrained_model_name_or_path="/home/yuan/debug_squad")

model = AutoModelForQuestionAnswering.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    config=model_config
)

# %% Load The Model and its utils 
model_name = 'bert-large-uncased'

tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForQuestionAnswering.from_pretrained(model_name)
# model_config = BertConfig.from_pretrained(pretrained_model_name_or_path="/home/yuan/debug_squad")


#%%
from datasets import load_dataset
raw_datasets = load_dataset('squad')
column_names = raw_datasets["train"].column_names
question_column_name = "question" if "question" in column_names else column_names[0]
context_column_name = "context" if "context" in column_names else column_names[1]
answer_column_name = "answers" if "answers" in column_names else column_names[2]

# %% Training preprocessing
def prepare_train_features(examples):
    pad_on_right = 1
    max_seq_length = 384

    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples[question_column_name] = [q.lstrip() for q in examples[question_column_name]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples[question_column_name if pad_on_right else context_column_name],
        examples[context_column_name if pad_on_right else question_column_name],
        truncation="only_second" if pad_on_right else "only_first",
        max_length=max_seq_length,
        # stride=data_args.doc_stride,
        # return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")
    # The offset mappings will give us a map from token to character position in the original context. This will
    # help us compute the start_positions and end_positions.
    offset_mapping = tokenized_examples.pop("offset_mapping")

    # Let's label those examples!
    tokenized_examples["start_positions"] = []
    tokenized_examples["end_positions"] = []

    for i, offsets in enumerate(offset_mapping):
        # We will label impossible answers with the index of the CLS token.
        input_ids = tokenized_examples["input_ids"][i]
        cls_index = input_ids.index(tokenizer.cls_token_id)

        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        answers = examples[answer_column_name][sample_index]
        # If no answers are given, set the cls_index as answer.
        if len(answers["answer_start"]) == 0:
            tokenized_examples["start_positions"].append(cls_index)
            tokenized_examples["end_positions"].append(cls_index)
        else:
            # Start/end character index of the answer in the text.
            start_char = answers["answer_start"][0]
            end_char = start_char + len(answers["text"][0])

            # Start token index of the current span in the text.
            token_start_index = 0
            while sequence_ids[token_start_index] != (1 if pad_on_right else 0):
                token_start_index += 1

            # End token index of the current span in the text.
            token_end_index = len(input_ids) - 1
            while sequence_ids[token_end_index] != (1 if pad_on_right else 0):
                token_end_index -= 1

            # Detect if the answer is out of the span (in which case this feature is labeled with the CLS index).
            if not (offsets[token_start_index][0] <= start_char and offsets[token_end_index][1] >= end_char):
                tokenized_examples["start_positions"].append(cls_index)
                tokenized_examples["end_positions"].append(cls_index)
            else:
                # Otherwise move the token_start_index and token_end_index to the two ends of the answer.
                # Note: we could go after the last offset if the answer is the last word (edge case).
                while token_start_index < len(offsets) and offsets[token_start_index][0] <= start_char:
                    token_start_index += 1
                tokenized_examples["start_positions"].append(token_start_index - 1)
                while offsets[token_end_index][1] >= end_char:
                    token_end_index -= 1
                tokenized_examples["end_positions"].append(token_end_index + 1)

    return tokenized_examples

    # Here I define the Max train samples to limited the preprocessing time
max_train_samples = 1
if 1:
    if "train" not in raw_datasets:
        raise ValueError("--do_train requires a train dataset")
    train_dataset = raw_datasets["train"]
        # We will select sample from whole data if agument is specified
    train_dataset = train_dataset.select(range(max_train_samples))
    # Create train feature from dataset
    train_dataset = train_dataset.map(
            prepare_train_features,
            batched=True,
            num_proc=1,
            remove_columns=column_names,
            load_from_cache_file=False,
            desc="Running tokenizer on train dataset",
    )
        # Number of samples might increase during Feature Creation, We select only specified max samples
    # train_dataset = train_dataset.select(range(max_train_samples))   # Training preprocessing
# %% 
  # Validation preprocessing
def prepare_validation_features(examples):
    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples[question_column_name] = [q.lstrip() for q in examples[question_column_name]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples[question_column_name],
        examples[context_column_name],
        truncation="only_second",
        max_length=384,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")

    # For evaluation, we will need to convert our predictions to substrings of the context, so we keep the
    # corresponding example_id and we will store the offset mappings.
    tokenized_examples["example_id"] = []

    for i in range(len(tokenized_examples["input_ids"])):
        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)
        context_index = 1 

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        tokenized_examples["example_id"].append(examples["id"][sample_index])

        # Set to None the offset_mapping that are not part of the context so it's easy to determine if a token
        # position is part of the context or not.
        tokenized_examples["offset_mapping"][i] = [
            (o if sequence_ids[k] == context_index else None)
            for k, o in enumerate(tokenized_examples["offset_mapping"][i])
        ]

    return tokenized_examples


# %% Load Validation Dataset 
seq_index = 6
eval_example = raw_datasets['validation']
eval_dataset = eval_example.map(
    prepare_validation_features,
    batched=True,
    remove_columns=column_names,
    load_from_cache_file=False,
    desc='asd'
).remove_columns(['offset_mapping', 'example_id']).select(range(seq_index-1, seq_index+2))
from torch.utils.data import SequentialSampler, DataLoader, dataloader
eval_sampler = SequentialSampler(eval_dataset)
eval_dataloader = DataLoader(eval_dataset, sampler=eval_sampler)

inputs = {
       "input_ids": torch.as_tensor(eval_dataset['input_ids'], device='cuda'),
       "attention_mask": torch.as_tensor(eval_dataset['attention_mask'], device='cuda'),
       "token_type_ids": torch.as_tensor(eval_dataset['token_type_ids'], device='cuda')
        }

seq_words0 = tokenizer.convert_ids_to_tokens(eval_dataset[0]['input_ids'])
seq_words1 = tokenizer.convert_ids_to_tokens(eval_dataset[1]['input_ids'])
seq_words2 = tokenizer.convert_ids_to_tokens(eval_dataset[2]['input_ids'])


# %% Feed the data
model = model.cuda()
model.eval()
model(**inputs)
# outputs = model(**inputs)


# %% Check the similar

import torch
def sim_func(t1: torch.Tensor, t2: torch.Tensor):
    """
    S(A, A') = 1 - 1/n * Sigma( 1/2 || A[p,:] , A'[p,:] ||)
    sim score C_lh = max_{h'} * 1/T * Sigma (S)
    @parms
    t1: [ _seq_len, _seq_len]
    t2: [ _seq_len, _seq_len]
    @return 
    sim
    """
    seq_len = t1.shape[1]
    res = 0
    for i in range(seq_len):
        res += 0.5 * (t1[i]-t2[i]).abs().sum()
    res /= seq_len
    return (1-res).detach().item()

def l1_norm(t1: torch.Tensor, t2: torch.Tensor):
    return 0.5 * (t1 - t2).abs().sum().detach().item() / (t1.shape[1])

# %%
sim_between_0_1 = {}
sim_between_1_2 = {}

for layer_num in range(24):
    t0 = model.bert.encoder.layer[layer_num].attention.self.attention_probs[0]
    t1 = model.bert.encoder.layer[layer_num].attention.self.attention_probs[1]
    sim_between_0_1[layer_num] = []
    for head_num in range(12):
        sim_between_0_1[layer_num].append(\
            sim_func(t0[head_num, 13: 13+156, 13:13+156],\
                 t1[head_num, 13: 13+156, 13:13+156]))



for layer_num in range(24):
    t0 = model.bert.encoder.layer[layer_num].attention.self.attention_probs[0]
    t1 = model.bert.encoder.layer[layer_num].attention.self.attention_probs[2]
    sim_between_1_2[layer_num] = []
    for head_num in range(12):
        sim_between_1_2[layer_num].append(\
            sim_func(t0[head_num, 13: 13+156, 13:13+156],\
                 t1[head_num, 10: 10+156, 10:10+156]))

# %%
attn_score_sim_between_0_1 = {}
attn_score_sim_between_1_2 = {}

for layer_num in range(12):
    t0 = model.bert.encoder.layer[layer_num].attention.self.attention_scores[0]
    t1 = model.bert.encoder.layer[layer_num].attention.self.attention_scores[1]
    attn_score_sim_between_0_1[layer_num] = []
    for head_num in range(12):
        attn_score_sim_between_0_1[layer_num].append(\
            l1_norm(t0[head_num, 13: 13+156, 13:13+156],\
                 t1[head_num, 13: 13+156, 13:13+156]))



for layer_num in range(12):
    t0 = model.bert.encoder.layer[layer_num].attention.self.attention_scores[0]
    t1 = model.bert.encoder.layer[layer_num].attention.self.attention_scores[2]
    attn_score_sim_between_1_2[layer_num] = []
    for head_num in range(12):
        attn_score_sim_between_1_2[layer_num].append(\
            l1_norm(t0[head_num, 13: 13+156, 13:13+156],\
                 t1[head_num, 10: 10+156, 10:10+156]))
# %% reproduce google result
# %%
same_sequence_sim_in_adj_layers = {}


for layer_num in range(11):
    t0 = model.bert.encoder.layer[layer_num].attention.self.attention_probs[0]
    t1 = model.bert.encoder.layer[layer_num+1].attention.self.attention_probs[0]
    same_sequence_sim_in_adj_layers[layer_num] = []
    for head_num in range(12):
        same_sequence_sim_in_adj_layers[layer_num].append(\
            sim_func(t0[head_num, 13: 13+156, 13:13+156],\
                 t1[head_num, 13: 13+156, 13:13+156]))

# %%
