export CUDA_VISIBLE_DEVICES=""
export TASK_NAME=sst2
  # --model_name_or_path gchhablani/bert-base-cased-finetuned-cola \

python run_glue.py \
  --model_name_or_path /home/yuanfeng/models/512/$TASK_NAME \
  --task_name sst2 \
  --do_train 0\
  --do_eval 1\
  --max_seq_length 512 \
  --per_device_train_batch_size 1 \
  --per_device_eval_batch_size  64 \
  --learning_rate 2e-5 \
  --num_train_epochs 2 \
  --output_dir /home/yuanfeng/debug/512/$TASK_NAME/

# python run_glue.py \
#   --model_name_or_path Bhumika/roberta-base-finetuned-sst2 \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_size 1 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuanfeng/debug/roberta/256/$TASK_NAME/


# python run_glue.py \
#   --model_name_or_path  /home/yuanfeng/debug/deberta/512/sst2 \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 1 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir  /home/yuanfeng/debug/deberta/512/sst2

# python run_glue.py \
#   --model_name_or_path  /home/yuanfeng/debug/deberta/256/sst2 \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512\
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 512 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir  /home/yuanfeng/debug/deberta/256/sst2

# python run_glue.py \
#   --model_name_or_path /home/yuanfeng/debug/prunebert/ \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 64 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir  /home/yuanfeng/debug/prunebert/ \
