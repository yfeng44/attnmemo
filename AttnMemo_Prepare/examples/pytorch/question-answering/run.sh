# python run_qa.py \
#   --model_name_or_path /home/yuan/debug/256/squad \
#   --dataset_name squad \
#   --do_train 0 \
#   --do_eval 1 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_siz 64 \
#   --learning_rate 3e-5 \
#   --num_train_epochs 2 \
#   --max_seq_length 256\
#   --doc_stride 128 \
#   --output_dir /home/yuan/debug/256/squad/


export CUDA_VISIBLE_DEVICES=""

python run_qa.py \
  --model_name_or_path valhalla/longformer-base-4096-finetuned-squadv1 \
  --dataset_name squad \
  --do_train 0 \
  --do_eval 1 \
  --per_device_train_batch_size 64 \
  --per_device_eval_batch_siz 64\
  --learning_rate 3e-5 \
  --num_train_epochs 2 \
  --max_seq_length 4096 \
  --doc_stride 128 \
  --output_dir /home/yuan/debug/4096/squad/
