#include <torch/torch.h> // libtorch
#include <stdio.h>
#include <ctype.h>
#include <sys/mman.h> /*mmap munmap*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <unordered_set>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>


using namespace torch; // libtorch
using namespace std;
namespace py = pybind11;
using namespace py::literals;

class TensorDB
{
public:
    TensorDB(string path);
    ~TensorDB();
    TensorDB(string path, int seqLen);
    TensorDB(string path, int attnHeads, int seqlen);
    int put(int id, Tensor &tensor);
    void unBindMem();
    const Tensor& get(int id);
    // void clearGet();
    const Tensor& multiGet(py::array_t<int> idx);
    // void clearMultiGet();
    const Tensor& compose(const py::array_t<int> &idx, const py::array_t<int> &reuse_idx, const py::array_t<int> &compute_idx, const Tensor &computeTensor);

private:
    string DBpath;
    int seqLen;
    int attnHeads;
    size_t page_size;
    size_t _1_dim_stride;
    size_t tensor_data_size;
    size_t reserve_data_size;

    float* dummy;

    torch::Tensor singleGetTensor;
    float *singleGetBuf;
    int batchSize;

    int singleGetBufFlag = 0;
    int multiGetBufFlag = 0;

    torch::Tensor multiGetTensor;
    std::vector<float *> bufs; // Here we make keep the ownership and the lifetime
};

TensorDB::TensorDB(string path)
{
    this->page_size = sysconf(_SC_PAGESIZE);
    this->DBpath = path;
    this->seqLen = 128;
    this->attnHeads = 12;
    this->tensor_data_size = this->seqLen * this->seqLen * this->attnHeads * sizeof(float);
}

TensorDB::TensorDB(string path, int seqLen)
{
    this->page_size = sysconf(_SC_PAGESIZE);
    this->DBpath = path;
    this->seqLen = seqLen;
    this->attnHeads = 12;
    this->tensor_data_size = this->seqLen * this->seqLen * this->attnHeads * sizeof(float);
}

TensorDB::TensorDB(string path, int attnHeads, int seqLen)
{
    this->page_size = sysconf(_SC_PAGESIZE);
    this->DBpath = path;
    this->seqLen= seqLen;
    this->attnHeads = attnHeads;
    this->tensor_data_size = this->seqLen * this->seqLen * this->attnHeads * sizeof(float);
}
int TensorDB::put(int id, torch::Tensor &t)
{
    float *buf;
    int fd;
    string fname = this->DBpath + std::to_string(id);
    fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd == -1)
    {
        perror("Open file error on fd open");
        return 1;
    }

    lseek(fd, this->tensor_data_size + 1, SEEK_SET);
    write(fd, "", 1);

    buf = (float *)mmap(NULL, tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (buf == MAP_FAILED)
    {
        perror("mmap fail");
    }

    if (close(fd) == -1)
    {
        perror("close fd");
        return 1;
    }

    float *b = t.data_ptr<float>();
    memcpy(buf, b, this->tensor_data_size);
    msync(buf, this->tensor_data_size, MS_SYNC);

    if (munmap(buf, tensor_data_size) == -1)
    {
        perror("munmap error");
        return 1;
    }
    return 0;
}

const torch::Tensor& TensorDB::get(int id)
{
    int fd;
    string fname = this->DBpath + std::to_string(id);
    fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd == -1)
    {
        perror("Open file error on fd open");
        return this->singleGetTensor;
    }
    // lseek(fd, this->tensor_data_size+1, SEEK_SET);
    // write(fd, "", 1);

    //if the buf is already mmaped
    if (this->singleGetBufFlag) {
        if (munmap(this->singleGetBuf, this->tensor_data_size) == -1) {
            perror("munmap error on Tensor single Get");
        }
    }
    this->singleGetBufFlag = 1;

    this->singleGetBuf = (float *)mmap(NULL, tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (this->singleGetBuf == MAP_FAILED)
    {
        perror("mmap fail on single GET");
    }

    if (close(fd) == -1)
    {
        perror("close fd on single GET");
        return this->singleGetTensor;
    }

    // float* b = t.data_ptr<float>();
    // memcpy(buf, b, this->tensor_data_size);
    // mcync(buf, this->tensor_data_size, MS_SYNC);

    // if (munmap(buf1, tensor_data_size) == -1) {
    //     perror("munmap error");
    //     return 1;
    // }
    auto options = torch::TensorOptions().dtype(torch::kFloat32);
    this->singleGetTensor = torch::from_blob(this->singleGetBuf, {1, this->attnHeads, this->seqLen, this->seqLen}, options);
    return this->singleGetTensor;
}

void TensorDB::unBindMem() {
    for (auto &i: this->bufs) {
        if (munmap(i, this->tensor_data_size) == -1) {
            perror("munmap error on TensorDB.bufs");
        }
    }

    if (this->multiGetBufFlag) {
        if (munmap(this->dummy, 16 * this->batchSize * this->tensor_data_size) == -1) {
            perror("munmap erro on this->dummy");
        }
    }
    
    this->bufs.clear();
}

const torch::Tensor& TensorDB::multiGet(py::array_t<int> idx) {
    // timeval start, end, endend;
    // gettimeofday(&start, NULL);


    py::buffer_info pybuf = idx.request();
    int size = pybuf.shape[0];
    int* ids = (int*)pybuf.ptr;

    this->unBindMem();
    this->batchSize = size;
    this->dummy = (float*) mmap(NULL, 16 * size * this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);

    string fname = this->DBpath + std::to_string(ids[0]);

    int fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd == -1) {
        perror("Open error on multi get");
        return this->multiGetTensor;
    }
    this->bufs.push_back(
        (float*) mmap(this->dummy, this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0) 
    );

    if (this->bufs[0] == MAP_FAILED) {
        perror("Mmap error in multiGEt");
        return this->multiGetTensor;
    }

    if (close(fd) == -1) {
        perror("close fd error on Multiget");
        return this->multiGetTensor;
    }
    for (int i = 1 ; i < size ; i++) {
        int fd;
        string fname = this->DBpath + std::to_string(ids[i]);

        fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
        if (fd == -1) {
            perror("Open error on multi get");
            return this->multiGetTensor;
        }

        this->bufs.push_back(
            (float*) mmap(this->bufs[i-1] + this->tensor_data_size, this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0)
        );

        if (this->bufs[i] == MAP_FAILED) {
            perror("Mmap error in multiGEt");
            return this->multiGetTensor;
        }

        if (close(fd) == -1) {
            perror("close fd error on Multiget");
            return this->multiGetTensor;
        }
    }
    // gettimeofday(&end, NULL);
    // cout << "Memory map/remap Elapsed Time" << end.tv_usec - start.tv_usec << "us" << endl;
    // printf("\n dummp = %p \n", dummy);
    // printf("\n buf0 = %p \n", this->bufs[0]);
    // printf("\n buf1 = %p \n", this->bufs[1]);
    // printf("\n buf2 = %p \n", this->bufs[2]);
    // printf("\n interval = %p \n", ((long unsigned int)(this->bufs[1]-this->bufs[0]))/sizeof(float));

    auto options = torch::TensorOptions().dtype(torch::kFloat32);
    this->multiGetTensor = torch::from_blob(
        this->bufs[0], 
        {size, this->attnHeads, this->seqLen, this->seqLen}, 
        { ((long unsigned int)(this->bufs[1]-this->bufs[0])), this->seqLen * this->seqLen, this->seqLen, 1}, 
        NULL, 
        options
        );
    // gettimeofday(&endend, NULL);
    // cout << "Total Elapsed Time" << endend.tv_usec - start.tv_usec << "us" << endl;

    return this->multiGetTensor;
}

const Tensor& TensorDB::compose(const py::array_t<int> &idx, const py::array_t<int> &reuse_idx, const py::array_t<int> &compute_idx, const Tensor &computeTensor) {
    py::buffer_info idx_buf = idx.request();
    py::buffer_info reuse_idx_buf = reuse_idx.request();
    int reuse_length = reuse_idx_buf.shape[0];
    py::buffer_info compute_idx_buf = compute_idx.request();
    int compute_length = compute_idx_buf.shape[0];

    this->unBindMem();
    int size =  reuse_idx_buf.shape[0] + compute_idx_buf.shape[0];
    this->batchSize = size;

    int* idx_arr = (int*)idx_buf.ptr;
    int* reuse_idx_arr = (int*)reuse_idx_buf.ptr;
    int* compute_idx_arr = (int*)compute_idx_buf.ptr;

    std::unordered_set<int> reuse_set(reuse_idx_arr, reuse_idx_arr + reuse_length);
    std::unordered_set<int> compute_set(compute_idx_arr, compute_idx_arr + compute_length);

    this->dummy = (float*) mmap(NULL, 16 * size * this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);
    int j = 0; // reuse 
    int k = 0; // compute

    if (reuse_set.count(0) == 1) { // we can find the 0 in the reuse set, so find the file obj and mmap
        string fname = this->DBpath + std::to_string(idx_arr[j]); //FIXME: BUG:FIX id[0] is not the first tensor!! 

        int fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
        if (fd == -1) {
            perror("Open error on multi get");
            return this->multiGetTensor;
        }
        this->bufs.push_back(
            (float*) mmap(this->dummy, this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0)
        );

        if (this->bufs[0] == MAP_FAILED) {
            perror("Mmap error in multiGEt");
            return this->multiGetTensor;
        }

        if (close(fd) == -1) {
            perror("close fd error on Multiget");
            return this->multiGetTensor;
        }
        j++;
    } else if (compute_set.count(0) == 1) {
        float *b = computeTensor.index({k}).data_ptr<float>();
        this->bufs.push_back(
            (float*) mremap(b, this->tensor_data_size, this->tensor_data_size, MREMAP_MAYMOVE | MREMAP_FIXED, this->dummy)//FIXME: Here the same
        );
        k++;
        if (this->bufs[0] == MAP_FAILED) {
            perror("Mremap error in 1st remap");
        }

    } else {
        perror("IDx 0 should be either in reuse set or in the compute set");
        return this->multiGetTensor;
    }

    for (int i = 1; i < size; i++) {
        if (reuse_set.count(i) == 1) { // we can find the 0 in the reuse set, so find the file obj and mmap
            string fname = this->DBpath + std::to_string(idx_arr[j]); //FIXME: BUG:FIX id[0] is not the first tensor!! 

            int fd = open(fname.c_str(), O_RDWR | O_CREAT, 0666);
            if (fd == -1) {
                perror("Open error on multi get");
                return this->multiGetTensor;
            }
            printf("\n buf[0] = %p", this->bufs[0]);
            printf("\n i = %d, buf[i-1] = %p", i, this->bufs[i-1]);
            this->bufs.push_back(
                (float*) mmap(this->bufs[i-1] + this->tensor_data_size, this->tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0) 
            );

            if (this->bufs[i-1] == MAP_FAILED) {
                perror("Mmap error in compose");
                return this->multiGetTensor;
            }

            if (close(fd) == -1) {
                perror("close fd error on compose");
                return this->multiGetTensor;
            }
            j++;
        } else if (compute_set.count(i) == 1) {
            float *b = computeTensor.index({k}).data_ptr<float>();
            printf("\n compute tensor b at %p", b);
            float* tmp = (float*) mremap(b, this->tensor_data_size, this->tensor_data_size, MREMAP_MAYMOVE | MREMAP_FIXED, this->bufs[i-1] + this->tensor_data_size);//FIXME: Here the same
            printf("\n tmp = %p", tmp);
            this->bufs.push_back(tmp);
            printf("\n i = %d, buf[i] = %p", i, this->bufs[i]);
            if (this->bufs[i-1] == MAP_FAILED) {
                perror("Mremap error ");
            }
            k++;
        } else {
            perror("IDx 0 should be either in reuse set or in the compute set!!");
            return this->multiGetTensor;
        }
    }

    printf("\n dummp = %p \n", dummy);
    printf("\n buf0 = %p \n", this->bufs[0]);
    printf("\n buf1 = %p \n", this->bufs[1]);
    printf("\n buf2 = %p \n", this->bufs[2]);
    auto options = torch::TensorOptions().dtype(torch::kFloat32);
    this->multiGetTensor = torch::from_blob(
        this->bufs[0], 
        {size, this->attnHeads, this->seqLen, this->seqLen}, 
        { ((long unsigned int)(this->bufs[1]-this->bufs[0])), this->seqLen * this->seqLen, this->seqLen, 1}, 
        NULL, 
        options
        );
    return this->multiGetTensor;
}

TensorDB::~TensorDB() {
    if (this->singleGetBufFlag == 1) {
        if (munmap(this->singleGetBuf, this->tensor_data_size) == -1) {
            perror("munmap error on singleGetbuf");
        }
    }

    for (auto &i: this->bufs) {
        if (munmap(i, this->tensor_data_size) == -1) {
            perror("munmap error on TensorDB.bufs");
        }
    }
}


PYBIND11_MODULE(TensorDB, m) {
    py::class_<TensorDB>(m, "TensorDB")
        .def(py::init<string,int,int>())
        .def("put", &TensorDB::put)
        .def("get", &TensorDB::get)
        .def("multiGet", &TensorDB::multiGet)
        .def("compose", &TensorDB::compose);
}

// int main() {
//     auto tdb = TensorDB("/dev/shm/yuan/", 12, 256);
//     torch::Tensor tmp0;

//     for (int i = 0 ; i < 10000; i ++) {
//         tmp0 = torch::randn({12, 256, 256}, torch::dtype(torch::kFloat32));
//         if (i == 200) {
//             cout << tmp0.index({3,7,22}).tensor_data() << endl; 
//         }
//         tdb.put(i, tmp0);
//     }
//     torch::Tensor b = tdb.get(200);
//     cout << "{200,3,7,22}:" << b.index({0,3,7,22}).tensor_data() << endl;
    

//     torch::Tensor zb = tdb.get(900);
//     cout << "{900,3,7,100}:" << zb.index({0,3,7,100}).tensor_data() << endl;
//     int a[64];
//     srand(0);

//     return 0;
// }