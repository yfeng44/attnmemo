from turtle import forward
import torch
import torch.nn as nn
import torch.nn.functional as F
import time
import faiss

class LinearNet(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        # self.fc1 = nn.Linear(98304, 128) #512
        self.fc1 = nn.Linear(49152, 128) #256
        self.fc2 = nn.Linear(128, 128)
        self.bn1 = nn.BatchNorm2d(1)
        self.bn2 = nn.BatchNorm1d(128)
    
    def forward_once(self, x):
        x = torch.unsqueeze(x,1)
        x = F.max_pool2d(x, 2)
        x = self.bn1(x)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = self.bn2(x)
        x = self.fc2(x)
        return x

    def forward(self, x1, x2):
        output1 = self.forward_once(x1)
        output2 = self.forward_once(x2)
        return output1, output2

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,1,3)
        self.conv2 = nn.Conv2d(1,1,3)
        self.fc1 = nn.Linear(23684, 128)
        self.dropout1 = nn.Dropout2d(0.25)
        self.dropout2 = nn.Dropout2d(0.25)

    def forward_once(self, x):
        x = torch.unsqueeze(x, 1)
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        return x

    def forward(self, x1, x2):
        output1 = self.forward_once(x1)
        output2 = self.forward_once(x2)
        return output1, output2

class Emb():
    def __init__(self, layer, gpu: bool = False) -> None:
        self.emb = LinearNet()
        if gpu:
            self.emb.cuda()
        self.layer = layer 
        if gpu:
            self.emb.load_state_dict(torch.load("/home/yuanfeng/embed_models/linear_layer_deberta_256_40000/layer_{}/checkpoints/model-epoch3.pth".format(layer)))
        else:
            self.emb.load_state_dict(torch.load("/home/yuanfeng/embed_models/linear_layer_deberta_256_40000/layer_{}/checkpoints/model-epoch3.pth".format(layer), map_location=torch.device('cpu')))
        self.emb.eval() 
    def embed(self, inputs):
        """
        return a numpy obj
        """
        return self.emb.forward_once(inputs).detach().cpu().numpy()

class VecDB():
    def __init__(self, d=128, nlist=512, m=8, bit=8) -> None:
        ''''
        d dimention number
        nlist n prob
        m 
        bit PQ 
        IndexIVFPQ for less memory
        '''
        quantizer = faiss.IndexFlatL2(d)
        # self.index = faiss.IndexIVFPQ(quantizer, d, nlist, m, bit)
        self.index = faiss.IndexIVFFlat(quantizer, d, nlist)
        # self.index = faiss.IndexFlatL2(d) 
    def train_index(self, embeddings):
        assert not self.index.is_trained
        self.index.train(embeddings)
        assert self.index.is_trained

    def add(self, embedding):
        self.index.add(embedding)

    def search(self, embedding, k=1, nprob=128):
        self.index.prob = nprob
        D, I = self.index.search(embedding, k)
        return D, I

    def load(self, path):
        self.index = faiss.read_index(path)
    
    # def save(self, layer):
    #     faiss.write_index(self.index, "/home/yuan/dbs/vec_IndexIVFFlat/train/{}.faiss".format(layer))
        
        