    # --dataset_config_name wikitext-2-raw-v1 \
# python run_clm.py \
#     --model_name_or_path /home/yuanfeng/debug/gpt2 \
#     --dataset_name wikitext \
#     --dataset_config_name wikitext-103-raw-v1 \
#     --do_train 0 \
#     --do_eval 1 \
#     --per_device_train_batch_size 4 \
#     --per_device_eval_batch_size 64 \
#     --output_dir /home/yuanfeng/debug/gpt2 \

python run_clm.py \
    --model_name_or_path decapoda-research/llama-7b-hf \
    --dataset_name wikitext \
    --dataset_config_name wikitext-2-raw-v1 \
    --do_train \
    --do_eval \
    --output_dir /tmp/test-clm