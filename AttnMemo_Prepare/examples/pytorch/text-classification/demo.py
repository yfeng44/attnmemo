import torch
import faiss
# where the magic happens
import faiss.contrib.torch_utils

a = torch.randn(64, 512, 768).cuda()

start = torch.cuda.Event(enable_timing=True)
end = torch.cuda.Event(enable_timing=True)

start.record()
b = torch.pca_lowrank(a)
end.record()

print(b[0].shape)
print(b[1].shape)
print(b[2].shape)

torch.cuda.synchronize()

print(start.elapsed_time(end))
