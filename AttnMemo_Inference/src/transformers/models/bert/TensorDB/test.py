import torch
from TensorDB import TensorDB
import numpy as np
import time
from tqdm import tqdm

if __name__ == '__main__':
    t = TensorDB("/dev/shm/yuan/0/", 12 ,512)
    # for i in tqdm(range(10000)):
    #     t.put(i, torch.rand(12, 512, 512))

    inx = np.random.randint(low=0, high=10000, size=64)
    start = time.time()
    tensor = t.multiGet(inx)
    print("Tensor Construction Time:", time.time() - start)
    # print(tensor[32])
    # print(tensor.dtype)

