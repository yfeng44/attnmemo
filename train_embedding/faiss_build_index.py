# %% imports 
import faiss
import numpy as np 
import pickle
from net import LinearNet, Net 
import torch
from tqdm import tqdm

# %% open Hidden states 
class hidden_data():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_eval():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_eval/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_eval_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_eval_256/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_overlap():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/self_attention_result/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_eval_512():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_eval_512/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_qnli_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_qnli_256/{}".format(self.layer) + "/" + str(index+1), 'rb') as f: data = pickle.load(f)
        return data 

class hidden_data_squad_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_squad_256/{}".format(self.layer) + "/" + str(index+1), 'rb') as f: data = pickle.load(f)
        return data 


class hidden_data_cola_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_cola_256/{}".format(self.layer) + "/" + str(index+1), 'rb') as f: data = pickle.load(f)
        return data 

class hidden_data_gpt2_1024():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_gpt2_1024/{}".format(self.layer) + "/" + str(index), 'rb') as f: data = pickle.load(f)
        return data 

class hidden_data_albert_sst2_512():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_albert_sst2_512/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_roberta_sst2_512():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_roberta_sst2_512/{}".format(self.layer) + "/" + str(index+1), 'rb') as f:
            data = pickle.load(f)
        return data 


class hidden_data_gptneo_1024():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_gptneo_1024/{}".format(self.layer) + "/" + str(index), 'rb') as f:
            data = pickle.load(f)
        return data 

class hidden_data_deberta_512():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_deberta_sst2_512/{}".format(self.layer) + "/" + str(index), 'rb') as f:
            data = pickle.load(f)
        return data 

# %%
class hidden_data_bert_sst2_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_bert_sst2_256/{}".format(self.layer) + "/" + str(index), 'rb') as f:
            data = pickle.load(f)
        return data


class hidden_data_deberta_sst2_256():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_deberta_sst2_256/{}".format(self.layer) + "/" + str(index), 'rb') as f:
            data = pickle.load(f)
        return data

class hidden_data_prune_sst2_512():
    def __init__(self, layer) -> None:
        self.layer = layer

    def get(self, index):
        with open("/home/yuan/dbs/hidden_states_prunebert_sst2_512/{}".format(self.layer) + "/" + str(index), 'rb') as f:
            data = pickle.load(f)
        return data 
# %% Define the embedding
class Emb():
    def __init__(self, layer) -> None:
        self.emb = LinearNet() 
        self.layer = layer 
        self.emb.load_state_dict(torch.load("/home/yuan/torch/train_embedding/linear_layer_512/layer_{}/checkpoints/model-epoch3.pth".format(layer)))
        self.emb.eval()
    def embed(self, inputs):
        """
        return a numpy obj
        """
        return self.emb.forward_once(torch.unsqueeze(torch.from_numpy(inputs), 0)).detach().numpy()
# %% VecDB
class VecDB():
    def __init__(self, d=128, nlist=128, m=8, bit=8) -> None:
        ''''
        d dimention number
        nlist n prob
        m 
        bit PQ 
        IndexIVFPQ for less memory
        '''
        quantizer = faiss.IndexFlatL2(d)
        # self.index = faiss.IndexIVFPQ(quantizer, d, nlist, m, bit)
        self.index = faiss.IndexIVFFlat(quantizer, d, nlist)
        # self.index = faiss.IndexFlatL2(d) 
    def train_index(self, embeddings):
        assert not self.index.is_trained
        self.index.train(embeddings)
        assert self.index.is_trained

    def add(self, embedding):
        self.index.add(embedding)

    def search(self, embedding, k=1, nprob = 128):
        self.index.nprob = nprob
        D, I = self.index.search(embedding, k)
        return D, I
    
    def load(self, path):
        self.index = faiss.read_index(path)
    
    # def save(self, layer, size):
    #     faiss.write_index(self.index, "/home/yuan/dbs/prunebert-512_vecdb/linear_vecdb_prunebert_sst2_512_{}/{}.faiss".format(size, layer))
        

        
# %% Stack the embedding together 
# for size in  40000, 60000:
#     for layer in range(12):
#         layer_tensor = np.array([])
#         hidden = hidden_data_bert_sst2_256(layer)
#         emb = Emb(layer)
#         for i in tqdm(range(size)):
#             if i == 0:
#                 layer_tensor =  emb.embed(hidden.get(i).tensor)
#             else:
#                 tmp = emb.embed(hidden.get(i).tensor)
#                 layer_tensor = np.vstack((layer_tensor, tmp))
#         vecdb = VecDB()
#         vecdb.train_index(layer_tensor)
#         vecdb.add(layer_tensor)
#         vecdb.save(layer, size)

# %% feed to VecDb Testing BERT/ROBERTA/DEBERTA
for layer in range(12):
    layer_tensor = np.array([])
    hidden = hidden_data_prune_sst2_512(layer)
    emb = Emb(layer)
    for i in tqdm(range(5000)):
        if i == 0:
            layer_tensor =  emb.embed(hidden.get(i).tensor)
        else:
            tmp = emb.embed(hidden.get(i).tensor)
            layer_tensor = np.vstack((layer_tensor, tmp))

        if i == 4999 :
            vecdb = VecDB()
            vecdb.train_index(layer_tensor)
            vecdb.add(layer_tensor)
            vecdb.save(layer, i+1)

        

# %% feed to VecDb Testing
import time
vecdb = VecDB()
vecdb.load("/home/yuan/dbs/bert_512_vecdb/linear_vecdb_bert_sst2_512_4000/0.faiss")
hidden = hidden_data_prune_sst2_512(0)
accumulated_time = 0
for i in range(1000):
    random_array = np.random.rand(64, 128).astype('float32')
    start  = time.time()
    vecdb.search(random_array)
    end = time.time()
    accumulated_time += (end - start)


print(accumulated_time/1000)



# %%
