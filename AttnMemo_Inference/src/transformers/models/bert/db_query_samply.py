import numpy as np
import time 
from tqdm import tqdm
class inMemDB:
    def __init__(self, layer_num, mode='w+') -> None:
        self.layer_num = layer_num
        self.data = np.memmap('/dev/shm/yuan/db/inmemdb_{}'.format(self.layer_num), dtype='float32', mode=mode, shape=(10000, 12, 512, 512))
        # madvice = ctypes.CDLL("libc.so.6").madvise
        # madvice.argtypes = [ctypes.c_void_p, ctypes.c_size_t, ctypes.c_int]
        # madvice.restype = ctypes.c_int
        # assert madvice(self.data.ctypes.data, self.data.size * self.data.dtype.itemsize, 1) == 0, "MADVISE FAILED"
        # self.data._mmap.madvise(mmap.MADV_HUGEPAGE)
        # self.data._mmap.madvise(mmap.MADV_RANDOM)
    
    def add(self, k, v):
        self.data[k] = v

    def get(self, k):
        return self.data[k]
    
    def multiget(self, ks):
        start = time.time()
        if (len(ks) == 1):
            return np.expand_dims(self.data[ks.squeeze()], axis=0)
        b = self.data[ks.squeeze()]
        print("Multiget Time: ", time.time() - start)
        return b
    
    def save(self):
        self.data.flush()

if __name__ == '__main__':
    b = inMemDB(2)
    # for i in tqdm(range(10000)):
    #     b.add(i, np.random.rand(12, 512, 512))

    start = time.time()
    for i in tqdm(range(100)):
        ks = np.random.randint(0, 10000, size=64).reshape(64,1)
        b.multiget(ks)

    end = time.time()
    print("Time: ", (end - start) / 100)