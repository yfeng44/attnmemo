# %%
import torch
from TensorDB import TensorDB
import numpy as np
import time
from tqdm import tqdm
# %%

def sim(t1, t2) ->float:
    seq_len = t1.shape[1]
    return np.sum(np.abs(t1-t2)) / seq_len / 2

best_sims = [np.array([]) for i in range(4)]


for inx, len in enumerate([16, 32, 64]):
    db_test = TensorDB("/dev/shm/yuan/10{}/".format(len), 12, len)
    db_train = TensorDB("/dev/shm/yuan/{}/".format(len), 12, len)
    for i in tqdm(range(640)):
        train = db_test.get(i).clone().detach().cpu().numpy()[0][0]
        best = 0
        for j in range(10000):
            test = db_train.get(j).clone().detach().cpu().numpy()[0][0]
            _simi = sim(train, test)
            best = max(best, _simi)
        best_sims[inx] = np.append(best_sims[inx], best)
        # print(best_sims[inx][-1], best_sims[inx].size)
# %%
best_sims[3] = np.array([])
db_test = TensorDB("/dev/shm/yuan/128/", 12, 128)
db_train = TensorDB("/dev/shm/yuan/1128/", 12, 128)
for i in tqdm(range(640)):
    test = db_test.get(i).clone().detach().cpu().numpy()[0][0]
    best = 0
    for j in range(10000):
        train = db_train.get(j).clone().detach().cpu().numpy()[0][0]
        _simi = sim(train, test)
        best = max(best, _simi)
    best_sims[3] = np.append(best_sims[3], best)
# %%
# 
for inx, arr in enumerate(best_sims):
    np.savetxt("/home/yuanfeng/transformers/src/transformers/models/bert/TensorDB/sensitivity/{}.csv".format(inx), arr, delimiter=",")

# %%
