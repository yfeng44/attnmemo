# %%
import numpy as np
from tqdm import tqdm
total = []
for i in range(12):
    total.append(np.array([]))
# total = np.array([])
for i in tqdm(range(640)):
    b = np.loadtxt("records/{}.csv".format(i), delimiter=" ")
    for i in range(12):
        b[i] = b[i] + i * 10000
        total[i] = np.append(total[i], b[i])
    # total = np.append(total, b)

# %%
import matplotlib
import numpy as np
import matplotlib.pyplot as plt

axes = plt.gca()

axes.set_ylim([0,5])
plt.hist(total[0], bins=100000)

# %%

import numpy as np
import matplotlib.pyplot as plt

n, bins, patches = plt.hist(total[0], bins=10000, density=False, facecolor='g')


plt.xlabel('Tensor Index', fontsize=22)
plt.ylabel('Frequency of Access', fontsize=22)
plt.xlim(0, 10000)
plt.ylim(0, 5)
plt.grid(True)
plt.show()
# %%
unique_elements, counts_elements = np.unique(total[0], return_counts=True)
print("Frequency of unique values of the said array:")
print(np.asarray((unique_elements, counts_elements)))
# %%

matplotlib.rcParams.update({'font.size': 22})


plt.figure(figsize=(16,4))
plt.xlabel(u'APM indices',fontsize=24)
plt.ylabel(u'Num of access times',fontsize=24)
#plt.bar(data[i for i in count.keys()],data[i for i in count.values()],alpha=0.6,width=0.8,facecolor='deeppink',edgecolor='darkblue',w=1,label='number of class')
# plt.bar(10000, total[0],width=0.8,edgecolor='#0077CC',lw=1)
plt.bar(unique_elements, counts_elements, width=0.8, edgecolor='#0077CC', lw=1)
fig=plt.gcf()
#plt.legend(loc=2)

# %%
