#include <iostream>
#include <torch/torch.h>    // libtorch头文件

#include <stdio.h>
#include <ctype.h>
#include <sys/mman.h> /*mmap munmap*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <cstring>


using namespace torch;  // libtorch命名空间
using namespace std;

int main() {
    torch::Tensor tensor = torch::eye(3).to(torch::kFloat32);;
    torch::Tensor tensor2 = torch::randn({3,3,3}, torch::dtype(torch::kFloat32));
    size_t page_size = sysconf(_SC_PAGESIZE);
    std::cout << tensor.tensor_data() << std::endl;
    std::cout << tensor2.sizes() << std::endl;

    size_t tensor_data_size = tensor2.sizes()[0] * tensor2.sizes()[1] * tensor2.sizes()[2] * sizeof(float);
    cout << "tensor_data_size=" << tensor_data_size << endl;
    cout << "Sizeof(float) = " << sizeof(float) << endl;
    size_t num_of_pages_per_tensor = (int)(tensor_data_size / page_size) + 1;
    cout << "num_of_pages_per_tensor=" << num_of_pages_per_tensor << endl;

    int fd1, fd2, fd3;
    float* buf1;
    float* buf2;
    float* buf3;
    off_t len;
    string fname1 = "./test_files/test1";
    string fname2 = "./test_files/test2";
    string fname3 = "./test_files/test3";


    fd1 = open(fname1.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd1 == -1)
    {
        perror("open");
        return 1;
    }

    fd2 = open(fname2.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd2 == -1)
    {
        perror("open");
        return 1;
    }

    fd3 = open(fname3.c_str(), O_RDWR | O_CREAT, 0666);
    if (fd3 == -1)
    {
        perror("open");
        return 1;
    }
    // if (fstat(fd, &sb) == -1)
    // {
    //     perror("fstat");
    //     return 1;
    // }
    lseek(fd1, tensor_data_size+1, SEEK_SET);
    write(fd1,"",1);

    lseek(fd2, tensor_data_size+1, SEEK_SET);
    write(fd2,"",1);

    lseek(fd3, tensor_data_size+1, SEEK_SET);
    write(fd3,"",1);

    int num_of_tensors = 3;
    float* dummy = (float*) mmap(NULL, 3 * num_of_tensors * page_size * num_of_pages_per_tensor, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);
    if (dummy == MAP_FAILED)
    {
        perror("mmap dummy");
        return 1;
    }
    // buf = mmap(0, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    buf1 = (float*) mmap(dummy, tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd1, 0);
    if (buf1 == MAP_FAILED)
    {
        perror("mmap buf1");
        return 1;
    }

    buf2 = (float*) mmap(buf1 + 4096, tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd2, 0);
    if (buf2 == MAP_FAILED)
    {
        perror("mmap buf2");
        return 1;
    }

    buf3 = (float*) mmap(buf2 + 4096, tensor_data_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd3, 0);
    if (buf3 == MAP_FAILED)
    {
        perror("mmap buf3");
        return 1;
    }
    printf("\n buf1 = %p \n", buf1);
    printf("\n buf2 = %p \n", buf2);
    printf("\n buf3 = %p \n", buf3);
    printf("\n buf1 - buf2 = %d \n", (int)(buf2 - buf1));

    if (close(fd1) == -1)
    {
        perror("close");
        return 1;
    }

    if (close(fd2) == -1)
    {
        perror("close");
        return 1;
    }


    if (close(fd3) == -1)
    {
        perror("close");
        return 1;
    }

    float* b = tensor2.data_ptr<float>();
    memcpy(buf1, b, tensor_data_size);
    memcpy(buf2, b, tensor_data_size);
    memcpy(buf3, b, tensor_data_size);

    msync(buf1, tensor_data_size, MS_SYNC);
    msync(buf2, tensor_data_size, MS_SYNC);
    msync(buf3, tensor_data_size, MS_SYNC);

    for (int x = 0; x < tensor.sizes()[0]; ++x)
    {
        for (int y = 0; y < tensor.sizes()[1]; ++y)
        {
            std::cout << b[y * tensor.sizes()[0] + x] << " ";
        }

        std::cout << std::endl;
    }

    for (int y = 0; y < tensor.sizes()[0]; ++y)
    {
        for (int x = 0; x < tensor.sizes()[1]; ++x)
        {
            std::cout << buf1[y * tensor.sizes()[0] + x] << " ";
        }

        std::cout << std::endl;
    }


    for (int y = 0; y < tensor.sizes()[0]; ++y)
    {
        for (int x = 0; x < tensor.sizes()[1]; ++x)
        {
            std::cout << buf2[y * tensor.sizes()[0] + x] << " ";
        }

        std::cout << std::endl;
    }

    for (int y = 0; y < tensor.sizes()[0]; ++y)
    {
        for (int x = 0; x < tensor.sizes()[1]; ++x)
        {
            std::cout << buf3[y * tensor.sizes()[0] + x] << " ";
        }

        std::cout << std::endl;
    }

    printf("\n interval = %p \n", ((long unsigned int)(buf2-buf1))/sizeof(float));
    auto options = torch::TensorOptions().dtype(torch::kFloat32);
    torch::Tensor blo = torch::from_blob(buf1, {num_of_tensors, 3, 3, 3}, {((long unsigned int)(buf2-buf1))*4/sizeof(float), 3*3, 3, 1}, NULL, options);
    cout << "BLO start" << endl;
    cout << blo.sizes()<< endl;

    // for (int y = 0; y < tensor.sizes()[0]; ++y)
    // {
    //     for (int x = 0; x < tensor.sizes()[1]; ++x)
    //     {
    //         std::cout << blo.data_ptr<float>()[y * tensor.sizes()[0] + x] << " ";
    //     }

    //     std::cout << std::endl;
    // }
    cout << blo.index({1, 0, 0, 0}).tensor_data() << endl;

    if (munmap(buf1, tensor_data_size ) == -1)
    {
        perror("munmap");
        return 1;
    }

    if (munmap(buf2, tensor_data_size) == -1)
    {
        perror("munmap");
        return 1;
    }

    if (munmap(buf3, tensor_data_size) == -1) {
        perror("munmap");
        return 1;
    }

    return 0;
}
