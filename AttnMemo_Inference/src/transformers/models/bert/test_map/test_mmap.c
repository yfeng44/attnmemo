// 1) map file
// 2) test the pointer
// 3) change it to tensor
// 4) blob
#include <stdio.h>
#include <ctype.h>
#include <sys/mman.h> /*mmap munmap*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int fd;
    int *buf1;
    int *buf2;
    off_t len;
    struct stat sb;
    char *fname = "./test2";

    fd = open(fname, O_RDWR | O_CREAT | O_TRUNC, 0777);
    if (fd == -1)
    {
        perror("open");
        return 1;
    }
    // if (fstat(fd, &sb) == -1)
    // {
    //     perror("fstat");
    //     return 1;
    // }
    lseek(fd, 128*sizeof(int)+1, SEEK_SET);
    write(fd,"",1);

    // buf = mmap(0, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    buf1 = (int*) mmap(NULL, 128*(sizeof(int)), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    buf2 = (int*) mmap(NULL, 128*(sizeof(int)), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (buf1 == MAP_FAILED)
    {
        perror("mmap");
        return 1;
    }

    if (buf2 == MAP_FAILED)
    {
        perror("mmap");
        return 1;
    }

    if (close(fd) == -1)
    {
        perror("close");
        return 1;
    }

    // for (len = 0; len < sb.st_size; ++len)
    // {
    //     buf[len] = toupper(buf[len]);
    //     /*putchar(buf[len]);*/
    // }

    for (int i = 0 ; i < 128 ; i++) {
        // *(buf1 + i) = i;
        printf("buf1[%d]=%d", i, buf1[i]);
    }

    for (int i = 0 ; i < 128 ; i++) {
        // *(buf1 + i) = i;
        printf("buf2[%d]=%d", i, buf2[i]);
    }

    printf("\n buf1 = %p \n", buf1);
    printf("\n buf2 = %p \n", buf2);

    if (munmap(buf1, 128 * sizeof(int)) == -1)
    {
        perror("munmap");
        return 1;
    }

    if (munmap(buf2, 128 * sizeof(int)) == -1)
    {
        perror("munmap");
        return 1;
    }
    return 0;
}