import numpy as np
import time

b = np.random.rand(10000, 12, 256, 256)


start = time.time()
for i in range(100):
    a = np.random.randint(0, 10000, 64)
    b = b[a]
end = time.time()

print((end - start) / 100)

