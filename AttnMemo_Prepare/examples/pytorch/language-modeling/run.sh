    # --dataset_config_name wikitext-2-raw-v1 \
# python run_clm.py \
#     --model_name_or_path /home/yuan/debug/gpt2 \
#     --dataset_name wikitext \
#     --dataset_config_name wikitext-103-raw-v1 \
#     --do_train 0 \
#     --do_eval 1 \
#     --per_device_train_batch_size 4 \
#     --per_device_eval_batch_size 1 \
#     --output_dir /home/yuan/debug/gpt2 \

export CUDA_VISIBLE_DEVICES=""
python run_clm.py \
    --model_name_or_path EleutherAI/gpt-neo-125M \
    --dataset_name wikitext \
    --dataset_config_name wikitext-103-raw-v1 \
    --do_train 0 \
    --do_eval 1 \
    --per_device_train_batch_size 4 \
    --per_device_eval_batch_size 1 \
    --output_dir /home/yuan/debug/gpt-neo \