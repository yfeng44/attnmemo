# export CUDA_VISIBLE_DEVICES=""
export TASK_NAME=sst2

#  --model_name_or_path /home/yuan/debug/256/$TASK_NAME \
# --model_name_or_path gchhablani/bert-base-cased-finetuned-cola \

# python run_glue.py \
#   --task_name $TASK_NAME  \
#   --model_name_or_path /home/yuan/debug/512/sst2 \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_size 64 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/512/sst2

# python run_glue.py \
#   --model_name_or_path /home/yuan/debug/256/qnli \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 256 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_size 64 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/256/$TASK_NAME/

# python run_glue.py \
#   --model_name_or_path Alireza1044/albert-base-v2-sst2 \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_size 1 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/albert/512/$TASK_NAME/

# python run_glue.py \
#   --model_name_or_path Bhumika/roberta-base-finetuned-sst2 \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 32 \
#   --per_device_eval_batch_size 64 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/roberta/512/$TASK_NAME/

# python run_glue.py \
#   --model_name_or_path /home/yuan/debug/deberta/512/$TASK_NAME \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 512 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/deberta/512/$TASK_NAME/

# python run_glue.py \
#   --model_name_or_path /home/yuan/debug/deberta/256/$TASK_NAME \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 1 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir /home/yuan/debug/deberta/256/$TASK_NAME/


## for pruned model
# python run_glue.py \
#   --model_name_or_path /home/yuan/debug/prunebert/ \
#   --task_name $TASK_NAME  \
#   --do_train 0 \
#   --do_eval 1 \
#   --max_seq_length 512 \
#   --per_device_train_batch_size 16 \
#   --per_device_eval_batch_size 1 \
#   --learning_rate 2e-5 \
#   --num_train_epochs 2 \
#   --output_dir  /home/yuan/debug/prunebert/ \


# for longformer
python run_glue.py \
  --model_name_or_path alanwang8/longformer-base-4096-finetuned-sst2 \
  --task_name $TASK_NAME  \
  --do_train 0 \
  --do_eval 1 \
  --max_seq_length 4096 \
  --per_device_train_batch_size 2 \
  --per_device_eval_batch_size 4 \
  --learning_rate 2e-5 \
  --num_train_epochs 2 \
  --output_dir  /home/yuan/debug/longformer/ \

