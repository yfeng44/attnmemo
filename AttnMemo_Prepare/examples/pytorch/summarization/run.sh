
export CUDA_VISIBLE_DEVICES=""
export TASK_NAME=samsum

python run_summarization.py \
    --model_name_or_path /tmp/tst-summarization \
    --do_train 0 \
    --do_eval \
    --dataset_name  samsum \
    --per_device_train_batch_size=4 \
    --per_device_eval_batch_size=1 \
    --overwrite_output_dir \
    --predict_with_generate \
    --save_steps=5000 \
    --output_dir /home/yuan/debug/t5/512/$TASK_NAME/ 