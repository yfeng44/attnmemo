# %%
from nvm.pmemobj import (create, PersistentObjectPool, PersistentList, PersistentDict,
                         PersistentObject)
import os
import sys 
import numpy as np

# %%
pool_fn = 'test_pynvm.pmem'

try:
    pool = create(pool_fn)
except OSError as err:
    print(err)
    sys.exit()

with pool:
    with pool.transaction():
        root = pool.root = pool.new(PersistentDict)
        root[0] = np.random.rand(12, 128, 128)
        root[1] = np.random.rand(12, 128, 128)
        root[2] = np.random.rand(12, 128, 128)

# %%
