import argparse
from statistics import mode
import torch 
import numpy as np
import time 
import matplotlib.pyplot as plt

from net import LinearNet
from constrastive import ContrastiveLoss
from torch.autograd import Variable
from torch.utils.data import Dataset 
from torch.utils.data import DataLoader
import os 
import pickle 
from itertools import combinations
import pyrocksdb


class HiddenStatesDataset(Dataset):
    def __init__(self, file_dir, layer_num, length) -> None:
        self.data_dir = file_dir
        self.layer_num = layer_num
        self.folder = self.data_dir + '/' + str(self.layer_num)
        self.length = length
    
    def __len__(self):
        return self.length
        # return len([name for name in os.listdir(self.folder)])
        return 5000 

    def __getitem__(self, index): 
        with open(self.folder + "/" + str(index), 'rb') as f: # TODO: change to index+1 if needed
            data = pickle.load(f)
        return {'tensor': data.tensor, 'sample': data.sample}

class DataGenerator:
    def __init__(self, layer=0) -> None:
        self.db = pyrocksdb.DB()
        opt = pyrocksdb.Options()
        opt.IncreaseParallelism()
        opt.OptimizeLevelStyleCompaction()
        opt.create_if_missing = True

        self.ropt = pyrocksdb.ReadOptions()
        s = self.db.open(opt, '/home/yuan/dbs/prunebert-sst2-512/attn_{}_0.db'.format(layer))
        assert(s.ok())

    @staticmethod
    def sim(t1, t2) ->float:
        seq_len = t1.shape[1]
        return np.sum(np.abs(t1-t2)) / seq_len / 2

    def label_compute(self, x0_label:str, x1_label:str):
        """
        x0_gt x0 ground truth
        """
        jt = self.db.get(self.ropt, bytes(x0_label + '-0', encoding='utf8'))
        assert(jt.status.ok())
        x0_gt = pickle.loads(jt.data)

        jt = self.db.get(self.ropt, bytes(x1_label + '-0', encoding='utf8'))
        assert(jt.status.ok())
        x1_gt = pickle.loads(jt.data)

        return self.sim(x0_gt, x1_gt)

    def create_pairs(self, inputs):
        x0_data = []
        x1_data = []
        labels = []
        data = list(combinations(inputs['tensor'], 2))
        sample = list(combinations(inputs['sample'], 2))
        for (x0, x1), (sample0, sample1) in zip(data, sample):
            x0_label = str(sample0.item())
            x1_label = str(sample1.item())
            # print(x0_label, x1_label)
            label = self.label_compute(x0_label, x1_label)

            x0_data.append(x0)
            x1_data.append(x1)
            labels.append(label)
        x0_data = torch.stack(x0_data)
        x1_data = torch.stack(x1_data)
        labels = torch.from_numpy(np.array(labels, dtype=np.float32))
        return x0_data, x1_data, labels

def main(layer, length):
    torch.manual_seed(1)
    parser = argparse.ArgumentParser()
    parser.add_argument('--epoch', '-e', type=int, default=3,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--batchsize', '-b', type=int, default=32,
                        help='Number of images in each mini-batch')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    args = parser.parse_args()
    args.cuda = not args.no_cuda and torch.cuda.is_available()
    print("Args: %s" % args)

    # model = Net()
    model = LinearNet()
    # model = SingleNet()
    if args.cuda:
        model.cuda() 

    learning_rate = 0.1
    momentum = 0.9

    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, 
                  momentum=momentum)

    # optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    hidden_states = HiddenStatesDataset('/home/yuan/dbs/hidden_states_prunebert_sst2_512', layer, length)


    loss_fn = ContrastiveLoss()

    if args.cuda:
        kwargs = {'num_workers':1, 'pin_memory':True}
    else:
        kwargs = {}

    train_loader = DataLoader(hidden_states, batch_size=32, shuffle=True, **kwargs)
    data_generator = DataGenerator(layer)
    def train(epoch):
        train_loss = []
        model.train() 
        start = time.time() 
        for batch_idx, inputs in enumerate(train_loader):
            x0, x1, labels = data_generator.create_pairs(inputs)
            if args.cuda:
                x0, x1, labels = x0.cuda(), x1.cuda(), labels.cuda()
            x0, x1, labels = Variable(x0), Variable(x1), Variable(labels)
            output1, output2 = model(x0, x1)
            loss = loss_fn(output1, output2, labels)
            train_loss.append(loss.item())
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 50 == 0:
                print("Layer: {}, Loss: {}, batch:{}, Epoch:{}, Time/batch = {}".format(layer, loss.item(), batch_idx, epoch, time.time()-start))
                start = time.time()
        
        torch.save(model.state_dict(), '/home/yuan/torch/train_embedding/linear_layer_prunebert_sst2_512_{}/layer_{}/checkpoints/model-epoch{}.pth'.format(length, layer,epoch))
        return train_loss
    
    train_loss = []
    for epoch in range(1, args.epoch + 1):
        train_loss.extend(train(epoch))
    
    plt.gca().cla()
    plt.plot(train_loss, label="layer {} train loss".format(layer))
    plt.xlabel("Iteration")
    plt.ylabel("Loss")
    plt.title("Training Loss Layer {} with NN".format(layer))
    plt.legend()
    plt.draw()
    plt.savefig('train_loss_layer_{}.png'.format(layer))
    plt.gca().clear()

if __name__ == "__main__" :
    start = time.time()
    li = [i for i in range(12)]


    start = time.time()
    for i in li:
        main(i, 5000)
    print("Training Time 5000 ",time.time() - start)
    # start = time.time()
    # for i in li:
    #     main(i, 20000)
    # print("Training Time 20000 ",time.time() - start)
    # start = time.time()
    # for i in li:
    #     main(i, 10000)
    # print("Training Time 10000 ",time.time() - start)
