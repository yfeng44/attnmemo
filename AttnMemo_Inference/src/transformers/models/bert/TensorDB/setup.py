from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension

setup(
    name='TensorDB',
    ext_modules=[
        CppExtension('TensorDB', ['TensorDB.cpp']),
    ],
    cmdclass={
        'build_ext': BuildExtension
    })