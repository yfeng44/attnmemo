# %% imports
from random import sample
from statistics import variance
import pyrocksdb
import pickle
import numpy as np
import time
from tqdm import trange
from matplotlib import pyplot as plt
# %% Total Variants in Numpy 
def calSim(t1, t2) -> float:
    """
    S(A, A') = 1 - 1/n * Sigma( 1/2 || A[p,:] , A'[p,:] ||)
    sim score C_lh = max_{h'} * 1/T * Sigma (S)
    @parms
    t1: [ _seq_len, _seq_len]
    t2: [ _seq_len, _seq_len]
    @return 
    sim
    """
    seq_len = t1.shape[1]
    res = 0
    for i in range(seq_len):
        # res += 0.5 * (t1[i]-t2[i]).abs().sum()
        res += 0.5  * np.sum(np.abs(t1[i] - t2[i]))
    res /= seq_len
    return (1-res).item()

def sim(t1, t2) -> float: 
    seq_len = t1.shape[1]
    return 1 - np.sum(np.abs(t1-t2)) / seq_len / 2

# %%
layer_list = [0,1,2,3,4,5,6,7,8,9,10,11]
db1 = {i: pyrocksdb.DB() for i in layer_list}
db2 = {i: pyrocksdb.DB() for i in layer_list}
opts = pyrocksdb.Options()
opts.IncreaseParallelism()
opts.OptimizeLevelStyleCompaction()
opts.create_if_missing = True

ropts = pyrocksdb.ReadOptions()

for layer_num in layer_list:
    layer = 'attn_{}_{}.db'.format(layer_num, 0)
    s = db1[layer_num].open(opts, '/home/yuan/dbs/sst-128/{}'.format(layer))
    b = db2[layer_num].open(opts, '/home/yuan/dbs/sst-128/infer/{}'.format(layer))
    assert(s.ok())
    assert(b.ok())

map_infer_to_similar_train = {k:{} for k in layer_list}

for layer_num in layer_list:
    opts_i = pyrocksdb.ReadOptions()
    it = db1[layer_num].iterator(opts_i)

    it.seek_to_first()
    assert(it.status().ok())
    assert(it.valid())

    a = np.array([], dtype=np.float32)
    start = time.time()
    for j in trange(1, 601):
        highest = ('-1', -100)
        print(j)
        jt_key = bytes(str(j)+'-0', encoding='utf8')
        jt = db2[layer_num].get(ropts, jt_key)
        assert(jt.status.ok())
        jt_value = pickle.loads(jt.data)
        it.seek_to_first()
        tmp = np.array([], dtype=np.float32)
        assert(it.status().ok())
        assert(it.valid())
        while(it.valid()):
            sim_tmp = sim(pickle.loads(it.value()), jt_value)
            if sim_tmp > highest[1]:
                highest = (it.key(), sim_tmp)
            it.next()
        map_infer_to_similar_train[layer_num][jt_key] = highest


with open('/home/yuan/dbs/sst-512/map_infer_to_similar_train_whole.log', 'wb') as fw:
    pickle.dump(map_infer_to_similar_train, fw)
# %% Analysis the result 
with open('/home/yuan/dbs/sst-128/map_infer_to_similar_train_whole.log', 'rb') as fw:
    map_infer = pickle.load(fw)

# let's draw the distribution of the most-similar attention score we can find in the populated DB
layer = 11
lst = [map_infer[layer][k][1] for k in map_infer[layer].keys()]
plt.hist(lst, range=[0,1])
plt.legend()
plt.title("Layer #{}".format(layer))
plt.ylabel('No of attention scores', size='x-large')
plt.xlabel('Similarity', size= 'x-large')

# %%
with open('/home/yuan/dbs/sst-128/map_infer_to_similar_train_whole.log', 'rb') as fw:
    map_infer = pickle.load(fw)
layer_list = [0,1,2,3,4,5,6,7,8,9,10,11]
db1 = {i: pyrocksdb.DB() for i in layer_list}
db2 = {i: pyrocksdb.DB() for i in layer_list}
opts = pyrocksdb.Options()
opts.IncreaseParallelism()
opts.OptimizeLevelStyleCompaction()
opts.create_if_missing = True

ropts = pyrocksdb.ReadOptions()

for layer_num in layer_list:
    layer = 'attn_{}_{}.db'.format(layer_num, 0)
    s = db1[layer_num].open(opts, '/home/yuan/dbs/sst-128/{}'.format(layer))
    b = db2[layer_num].open(opts, '/home/yuan/dbs/sst-128/infer/{}'.format(layer))
    assert(s.ok())
    assert(b.ok())


# %%
tmp1 = pickle.loads(db1[2].get(ropts, b'3505-0').data)
tmp2 = pickle.loads(db2[2].get(ropts, b'23-0').data)


# %% How mutch over the threshold
for layer in range(11):
    tmp_ori = 0
    tmp = 0
    num = 0
    for key in map_infer[layer].keys():
        if map_infer[layer][key][1] > 0.9:
            num += 1
            tmp_ori += map_infer[layer][key][1]
            similar_record = map_infer[layer][key][0]
            tmp1 = pickle.loads(db1[layer+1].get(ropts, similar_record).data)
            tmp2 = pickle.loads(db2[layer+1].get(ropts, key).data)
            tmp += sim(tmp1, tmp2)
    tmp = tmp/num
    tmp_ori = tmp_ori / num
    print("Layer:", layer)
    print("Tmp={}, Tmp_ori={}, Total={}".format(tmp, tmp_ori,num))

# %% test the variance between the NN search and the Brutal force search 
import statistics
import re
layer = 0
ave1 = np.array([])
ave2 = np.array([])
ave3 = np.array([])
err1 = np.array([])
err2 = np.array([])
err3 = np.array([])
median1 = np.array([])
median2 = np.array([])
median3 = np.array([])

for layer in range(12):
    tmp_ave1 = np.array([])
    tmp_ave2 = np.array([])
    tmp_ave3 = np.array([])

    with open('/home/yuan/dbs/difference_exact_match_vs_nn_search/{}'.format(layer), 'r') as f:
        lines = f.readlines()

    dbt = pyrocksdb.DB()
    dbi = pyrocksdb.DB() 
    ropts = pyrocksdb.ReadOptions()
    opts = pyrocksdb.Options()
    opts.IncreaseParallelism()
    opts.OptimizeLevelStyleCompaction()
    opts.create_if_missing = True

    s = dbt.open(opts, '/home/yuan/dbs/sst-128/attn_{}_0.db'.format(layer))
    b = dbi.open(opts, '/home/yuan/dbs/sst-128/infer/attn_{}_0.db'.format(layer))
    assert(s.ok())
    assert(b.ok())

    with open('/home/yuan/dbs/difference_exact_match_vs_nn_search/ffnn_{}'.format(layer), 'rb') as h:
        ffnn = pickle.load(h)
    if (layer == 11):
        for line in lines:
            pairs = re.split(': |, |:| |\'|-', line)
            sample_num = pairs[1]
            t1_num = int(pairs[5])
            tmpt1 = pickle.loads(dbt.get(ropts, bytes("{}-0".format(t1_num), encoding='utf8')).data)
            tmpi  = pickle.loads(dbi.get(ropts, bytes("{}-0".format(sample_num), encoding='utf8')).data)
            tmp_ave1 = np.append(tmp_ave1, sim(tmpt1, tmpi))

        ave1 = np.append(ave1, np.mean(tmp_ave1))
        err1 = np.append(err1, np.std(tmp_ave1))
        median1 = np.append(median1, np.median(tmp_ave1))
        break
    i = 0
    for line in lines:
        pairs = re.split(': |, |:| |\'|-', line)
        sample_num = pairs[1]
        t1_num = int(pairs[5])
        t2_num = int(pairs[11]) + 1
        t3_num = int(ffnn[i][0]) + 1
        tmpt1 = pickle.loads(dbt.get(ropts, bytes("{}-0".format(t1_num), encoding='utf8')).data)
        tmpt2 = pickle.loads(dbt.get(ropts, bytes("{}-0".format(t2_num), encoding='utf8')).data)
        tmpt3 = pickle.loads(dbt.get(ropts, bytes("{}-0".format(t3_num), encoding='utf8')).data)
        tmpi  = pickle.loads(dbi.get(ropts, bytes("{}-0".format(sample_num), encoding='utf8')).data)
        tmp_ave1 = np.append(tmp_ave1, sim(tmpt1, tmpi))
        tmp_ave2 = np.append(tmp_ave2, sim(tmpt2, tmpi))
        tmp_ave3 = np.append(tmp_ave3, sim(tmpt3, tmpi))
        i += 1

    ave1 = np.append(ave1, np.mean(tmp_ave1))
    ave2 = np.append(ave2, np.mean(tmp_ave2))
    ave3 = np.append(ave3, np.mean(tmp_ave3))
    err1 = np.append(err1, np.std(tmp_ave1))
    err2 = np.append(err2, np.std(tmp_ave2))
    median1 = np.append(median1, np.median(tmp_ave1))
    median2 = np.append(median2, np.median(tmp_ave2))

# ave1 = np.append(ave1, 0)
ave2 = np.append(ave2, 0)
ave3 = np.append(ave3, 0)
# err1 = np.append(err1, 0)
err2 = np.append(err2, 0)
err3 = np.append(err3, 0)
# median1 = np.append(median1, 0)
median2 = np.append(median2, 0)
# %% Plot the result
layers = [i for i in range(12)]
x = np.arange(len(layers))
width = 0.35
fig, ax = plt.subplots()
rects1 = ax.bar(x - width, ave1, width, label='Exhaustive search', yerr=err1)
rects2 = ax.bar(x , ave2, width, label='CNN Embedding-based search', yerr=err2)
rects2 = ax.bar(x + width, ave3, width, label='FCN Embedding-based search', yerr=err2)

ax.set_ylabel('Similarity between search results and ground truth\n (i.e., attention scores based on computation)', fontsize=17)
ax.set_xlabel('Layer number', fontsize=18)
# ax.set_title('Average Similarity with different search', fontsize=15)
ax.set_xticks(layers)
ax.legend(fontsize=18)

#ax.bar_label(rects1, padding=3)
#ax.bar_label(rects2, padding=3)

fig = plt.gcf()
fig.tight_layout()
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
fig.set_size_inches(16, 9)

plt.savefig('similarity_different_search.png')
plt.show()

ave1 # a np arr
ave2 # a np arr 


# %%

# layers = [i for i in range(12)]
# x = np.arange(len(layers))
# width = 0.35
# fig, ax = plt.subplots()
# rects1 = ax.bar(x - width/2, ave1, width, label='Exhaustive search', yerr=err1)
# rects2 = ax.bar(x + width/2, ave2, width, label='VectorDB Query', yerr=err2)

# ax.set_ylabel('Similarities compared with Ground Truth')
# ax.set_xlabel('Layer number')
# ax.set_title('Average Similarity in with different search')
# ax.set_xticks(layers)
# ax.legend(fontsize=15)

# #ax.bar_label(rects1, padding=3)
# #ax.bar_label(rects2, padding=3)

# fig = plt.gcf()
# fig.set_size_inches(16, 9)
# plt.xticks(fontsize=15)
# plt.yticks(fontsize=15)

# plt.savefig('similarity_different_search.png')
# plt.show()
# %%
